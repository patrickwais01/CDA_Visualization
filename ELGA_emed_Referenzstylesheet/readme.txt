************
ELGA REFERENZ-STYLESHEET e-Medikation
------------------------
Version: 1.8.1+20230801
************

Nutzungsbedingungen:
--------------------
Das "ELGA Referenz-Stylesheet e-Medikation" wird von der ELGA GmbH bis auf Widerruf unentgeltlich und nicht-exklusiv sowie zeitlich und �rtlich unbegrenzt, jedoch beschr�nkt auf Verwendungen f�r die Zwecke der "Clinical Document Architecture" (CDA) zur Verf�gung gestellt. Ver�nderungen f�r die lokale Verwendung sind zul�ssig. Derartige Ver�nderungen (sogenannte bearbeitete Fassungen) d�rfen ihrerseits publiziert und Dritten zur Weiterverwendung und Bearbeitung zur Verf�gung gestellt werden. 
Bei der Ver�ffentlichung von bearbeiteten Fassungen ist darauf hinzuweisen, dass diese auf Grundlage des von der ELGA GmbH publizierten "ELGA Referenz-Stylesheet" erstellt wurden.
Die Anwendung sowie die allf�llige Bearbeitung des "ELGA Referenz-Stylesheet" erfolgt in ausschlie�licher Verantwortung der AnwenderInnen. Aus der Ver�ffentlichung, Verwendung und/oder Bearbeitung k�nnen keinerlei Rechtsanspr�che gegen die ELGA GmbH erhoben oder abgeleitet werden.

************
Optionen:
---------

Das Verhalten des Referenzstylesheets kann �ber folgende Optionen gesteuert werden (Bitte in der Datei ein- oder auskommentieren):
- ShowRevisionMarks: eingef�gten und gel�schten Text anzeigen
- use external css: Externes CSS aktivieren
- print icon visibility: Druck-Icon ausblenden
- isdeprecated - Status des Dokuments (g�ltig oder storniert)
- strict mode: ung�ltige Dokumente werden nicht gerendert

************
Das ELGA Referenz-Stylesheet wurde erfolgreich mit folgenden XSLT Prozessoren getestet:
- Saxon, Versionen 6.5.5 und 9.6.0.7
- Xsltproc
- .NET 1.0
- MSXML 3.0

************
Known Issues
************
Optimale Darstellungsergebnisse werden bei Verwendung von Firefox erreicht.

- Bei der Darstellung mittels Internet Explorer ist zu beachten:
  Bei der Darstellung von lokal gespeicherten Dateien muss folgende Einstellung f�r JavaScript getroffen werden:
  Internetoptionen -> Erweitert -> unter Sicherheit: Ausf�hrung aktiver Inhalte in Dateien auf dem lokalen Computer zulassen
- Beim Speichern von eingebetteten Dateien wird im Internet Explorer wird keine Dateiendung an die Datei geh�ngt

- Microsoft Spartan unterst�tzt keine XSL Transformationen. Ein zu einer HTML-Datei transformiertes Stylesheet wird aber fehlerfrei angezeigt.

- Bei der Darstellung in den verschiedenen Browsern, m�ssen einige Einstellungen beachtet werden, damit das Stylesheet richtig angezeigt wird (siehe Changelog_CDA Visualization_2019-09)

************
Change-Log
************
1.8.1+20230801
siehe PDF Dokument "Changelog_CDA_Visualization_2023-08"

1.7.0+20230310
siehe PDF Dokument "Changelog_CDA_Visualization_2023-03"

1.6.0+20221128
siehe PDF Dokument "Changelog_CDA_Visualization_2022-11"

1.5.1+20220926
siehe PDF Dokument "Changelog_CDA_Visualization_2022-09"

1.4.1+20220602
siehe PDF Dokument "Changelog_CDA_Visualization_2022-06"

1.3.0+20210904
siehe PDF Dokument "Changelog_CDA_Visualization_2021-09"

1.2.0+20210615
siehe PDF Dokument "Changelog_CDA_Visualization_2021-06"

1.1.0+20210514
siehe PDF Dokument "Changelog_CDA_Visualization_2021-05_2"

1.0.0+20210329
siehe PDF Dokument "Changelog_CDA_Visualization_2021-03_1"

V1.05.001
siehe PDF Dokument "Changelog_CDA_Visualization_2021-03"

V1.04.003.1
siehe PDF Dokument "Changelog_CDA_Visualization_2020-10"

V1.04.001
siehe beiliegendes PDF Dokument "Changelog_CDA_Visualization_2020-04"

V1.03.004
siehe beiliegendes PDF Dokument "Changelog_CDA_Visualization_2019-11"

V1.03.003.3
siehe beiliegendes PDF Dokument "Changelog_CDA_Visualization_2019-10"

V1.03.003.1
siehe beiliegendes PDF Dokument "Changelog_CDA_Visualization_2019-09"

V1.03.002.4
siehe beiliegendes PDF Dokument "Changelog_CDA_Visualization_2019-05"

V1.02.004.2
- Es kann nun ein individueller Warnhinweis per Parameter angezeigt werden.
- Die Einstellung des Parameters wird am Ende des Dokuments angezeigt.
- Die Haupteintr�ge der Verordnungen und Abgaben werden nun in der Medikationsliste nach Datum (j�ngstes zuerst) sortiert.
- Typo im Stylesheet CSS behoben.

V1.02.003.1
- Zur Anzeige der GDA-ID wird nun auf assigningAuthorityName="GDA-Index" und assigningAuthorityName="GDA-Index" gepr�ft.
- Bei der Einzeldosierung wird nun die Mengenart der Dosierung angezeigt.
- Bei unbekannten Daten zum Verordner wird in der Medikationsliste nun �-� statt �unbekannt� angezeigt.

V1.2.002.1
- Korrektur: Details bei der ersten angezeigten Packung werden richtig angezeigt.

V1.02.001
- Die �SetId� wird nun in den Dokumentinformationen angezeigt.

V1.01.005
- Vom ELGA Value Set ELGA_MedikationMengenart_VS wird nun der Wert �einheit print� zur Anzeige verwendet.
- Die alternative Einnahme wird nun bei der Dosierung angezeigt.

V1.01.004
- Anzeige mit unbekannten Datumswerten wurde behoben.
- Die Anzeige der Sprachf�higkeit des Patienten wurde eingef�gt.
- Mehrere �Authenticator� ohne �legalAuthenticator� werden nun richtig angezeigt


V1.01.002
- Tabellen mit Spaltenbreiten gr��er als 100% werden nun besser skaliert.
- Die Anzeige der Dosierung wurde folgenderma�en verbessert:
*Leerzeichen zwischen Wert und Einheit und Frequenz (t�glich) eingef�gt
*1 als unit wird ignoriert
*Die geschweiften Klammern bei �{St�ck}�, �{Tablette}�, � werden entfernt.


V1.01.001
- Korrektur: Wenn f�r das Land (state) ein Nullflavor angegeben wurde, wurde ein Beistrich nach der Stadt angezeigt.
- Korrektur: Wenn ein Datums-Wert mit einem Nullflavor angegeben wurde, wurde ein "." angezeigt

V1.00.006
- Fu�noten wiederholten sich bei Tabellen �ber mehrere Seiten und werden nun nur mehr am Ende der Tabelle angezeigt.
- Im Stylesheet wurde der Tag version=�1.0� entfernt, um die Transformation mit Saxon9 wieder zu erm�glichen.

V1.00.004
- Korrektur: Manuelle Sortierung des Abholungsdatums

V1.00.002
- Tabellendarstellung: Korrekte Darstellung des Footers
- Familienname des Patienten fettgedruckt
- Tabellendarstellung - COLSPAN (zus�tzliche Attribute)

V1.00.001
Erstversion auf Basis des ELGA-Referenzstylesheets Version 1.05.002





